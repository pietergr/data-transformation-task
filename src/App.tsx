import React, { FunctionComponent } from 'react';
import './App.scss';
import { Provider } from 'react-redux';
import { store } from './store';
import { ControlPanel, GlencoreNavbar } from './ui';

const App: FunctionComponent<{}> = () => {

    return (<Provider store={store}>
        <GlencoreNavbar/>
        <ControlPanel/>
    </Provider>)
};

export default App;
