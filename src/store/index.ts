import { combineReducers, createStore } from 'redux';
import { productReducer } from '../product';
import { colourTransformationReducer } from '../colour-map';
import { ProductState } from '../product/product-reducers';
import { ColourTransformationState } from '../colour-map/colour-transformation-reducers';

export interface AppState {
    product: ProductState;
    transformations: ColourTransformationState;
}

export const store = createStore(combineReducers({
        product: productReducer,
        transformations: colourTransformationReducer
    }),
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);