import { AppState } from '../store';
import { createSelector } from 'reselect';
import { ProductState } from './product-reducers';
import { colourTransformationSelector } from '../colour-map/colour-transformation-selectors';
import { Product } from './index';
import { ColourTransformationState } from '../colour-map/colour-transformation-reducers';

const productSelector = (state: AppState) => state.product;

export const productArraySelector = createSelector(productSelector, (productState: ProductState) => productState
    ? Object.keys(productState).map((key: string) => productState[key])
    : []
);

export const transformedProductSelector = createSelector(
    [productArraySelector, colourTransformationSelector],
    (products: Product[], colourTransformations: ColourTransformationState) => products
        .map(({productName, colour, price}) => ({
        productName,
        colour: colourTransformations[colour] ?  colourTransformations[colour].colourTo : colour,
        price
    })));