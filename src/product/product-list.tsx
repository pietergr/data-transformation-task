import { Product } from './index';
import { FunctionComponent } from 'react';
import { Table } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import React from 'react';

type ProductListProps = {
    products: Product[],
    onDeleteProduct: (product: Product) => void;
    editable: boolean;
};

export const ProductList: FunctionComponent<ProductListProps> = ({ products, onDeleteProduct, editable }) => {
    return <Table>
        <thead>
            <tr>
                <th>Product name</th>
                <th>Colour</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
        {
            products.map((product: Product, idx: number) => (
                <tr key={ idx}>
                    <td>{ product.productName }</td>
                    <td>{ product.colour }</td>
                    <td>{ product.price }</td>
                    { editable && <td className="pointer text-danger" onClick={ () => onDeleteProduct(product) }>
                        <FontAwesomeIcon icon={ faTrash }/>
                    </td> }
                </tr>
            ))
        }
        </tbody>
    </Table>
};