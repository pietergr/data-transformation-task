import React, { ChangeEvent, MouseEvent, FunctionComponent, useState } from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { Product } from './index';

type ProductFormProps = {
    onSubmitProduct: (product: Product) => void;
};

const emptyProduct: Product = {
    productName: '',
    colour: '',
    price: ''
};

export const ProductForm: FunctionComponent<ProductFormProps> = ({ onSubmitProduct }) => {

    const [ { productName, colour, price }, setProduct ] = useState<Product>(emptyProduct);

    const changeProduct = (productPart: { productName?: string, colour?: string, price?: string }) => {
        setProduct(Object.assign({
            productName,
            colour,
            price
        }, productPart));
    };

    const submit = (e: MouseEvent) => {
        e.preventDefault();
        onSubmitProduct({
            productName,
            colour,
            price
        });
        setProduct(emptyProduct)
    };

    return (
        <Form>
            <FormGroup>
                <Label>Product name</Label>
                <Input
                    type="text"
                    value={productName}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => changeProduct({
                        productName: e.target.value
                    })}/>
            </FormGroup>
            <FormGroup>
                <Label>Colour</Label>
                <Input
                    type="text"
                    value={colour}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => changeProduct({
                        colour: e.target.value
                    })}/>
            </FormGroup>
            <FormGroup>
                <Label>Price</Label>
                <Input
                    type="text"
                    value={price}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => changeProduct({
                        price: e.target.value
                    })}/>
            </FormGroup>
            <Button
                color="info"
                disabled={!productName || !colour || !price}
                onClick={(e: MouseEvent<HTMLElement>) => submit(e)}>Add</Button>
        </Form>
    );
};