import { Product } from './index';
import { AppState } from '../store';
import { ColourTransformation } from '../colour-map';
import { transformedProductSelector } from './product-selectors';

describe('product transformation', () => {

    const emptyColourTransformation: ColourTransformation = {
        colourFrom: '',
        colourTo: ''
    };

    const initialAppState: AppState = {
        product: {
            'Apple iPhone 6s': {
                productName: 'Apple iPhone 6s',
                colour: 'Anthracite',
                price: 'CHF 769'
            },
            'Samsung Galaxy S8': {
                productName: 'Samsung Galaxy S8',
                colour: 'Midnight Black',
                price: 'CHF 569'
            },
            'Huawei P9': {
                productName: 'Huawei P9',
                colour: 'Mystic Silver',
                price: 'CHF 272'
            }
        },
        transformations: {
            selectedColourTransformation: emptyColourTransformation,
            'Mystic Silver': {
                colourFrom: 'Mystic Silver',
                colourTo: 'New Colour'
            }
        }
    };

    it('transforms a product colour', () => {
        const transformedData: Product[] = transformedProductSelector(initialAppState);
        expect(transformedData).toEqual([
            {
                productName: 'Apple iPhone 6s',
                colour: 'Anthracite',
                price: 'CHF 769'
            },
            {
                productName: 'Samsung Galaxy S8',
                colour: 'Midnight Black',
                price: 'CHF 569'
            },
            {
                productName: 'Huawei P9',
                colour: 'New Colour',
                price: 'CHF 272'
            }
        ])
    });

    it('does not result in clones when transforming', () => {
        const candidateCloneResult: AppState = {
            product: initialAppState.product,
            transformations: {
                ...initialAppState.transformations,
                'Midnight Black': {
                    colourFrom: 'Midnight Black',
                    colourTo: 'New Colour'
                }
            }
        };
        const transformedData: Product[] = transformedProductSelector(candidateCloneResult);
        expect(transformedData).toEqual([
            {
                productName: 'Apple iPhone 6s',
                colour: 'Anthracite',
                price: 'CHF 769'
            },
            {
                productName: 'Samsung Galaxy S8',
                colour: 'New Colour',
                price: 'CHF 569'
            },
            {
                productName: 'Huawei P9',
                colour: 'New Colour',
                price: 'CHF 272'
            }
        ])
    });

});