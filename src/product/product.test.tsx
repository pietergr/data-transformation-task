import { productReducer, ProductState } from './product-reducers';
import { Product } from './index';
import { getDeleteProductAction, getSubmitProductAction } from './product-actions';

describe('product state', () => {

    const initialState: ProductState = {};

    const newProduct: Product = {
        productName: 'Apple iPhone 6s',
        colour: 'Anthracite',
        price: 'CHF 769'
    };

    it('adds a product', () => {
        const newState = productReducer(initialState, getSubmitProductAction(newProduct));
        expect(newState).toEqual({
            'Apple iPhone 6s': {
                productName: 'Apple iPhone 6s',
                colour: 'Anthracite',
                price: 'CHF 769'
            }
        });
    });

    it('deletes a product', () => {
        const newState = productReducer(initialState, getSubmitProductAction(newProduct));
        const finalState = productReducer(newState, getDeleteProductAction(newProduct));
        expect(finalState).toEqual({});
    });

    it('does not result in clones when adding a product', () => {
        const newState = productReducer(initialState, getSubmitProductAction(newProduct));
        const finalState = productReducer(newState, getSubmitProductAction(newProduct));
        expect(finalState).toEqual({
            'Apple iPhone 6s': {
                productName: 'Apple iPhone 6s',
                colour: 'Anthracite',
                price: 'CHF 769'
            }
        });
    });

});