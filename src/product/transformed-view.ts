import { AppState } from '../store';
import { transformedProductSelector } from './product-selectors';
import { Dispatch } from 'redux';
import { getDeleteProductAction, ProductAction } from './product-actions';
import { Product } from './index';
import { connect } from 'react-redux';
import { ProductList } from './product-list';

const mapStateToProps = (state: AppState) => ({
    products: transformedProductSelector(state),
    editable: false
});

const mapDispatchToProps = (dispatch: Dispatch<ProductAction>) => ({
    onDeleteProduct: (product: Product) => dispatch(getDeleteProductAction(product))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductList);