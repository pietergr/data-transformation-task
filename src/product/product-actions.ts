import { Product } from './index';
import { Action, ActionCreator } from 'redux';

export enum ProductActionTypes {
    SUBMIT_PRODUCT = '[ProductActionTypes] Submit Product',
    DELETE_PRODUCT = '[ProductActionTypes] Delete Product'
}

export interface ProductAction extends Action {
    product: Product
}

export const getSubmitProductAction: ActionCreator<ProductAction> = (product: Product) => ({
    type: ProductActionTypes.SUBMIT_PRODUCT,
    product
});

export const getDeleteProductAction: ActionCreator<ProductAction> = (product: Product) => ({
    type: ProductActionTypes.DELETE_PRODUCT,
    product
});