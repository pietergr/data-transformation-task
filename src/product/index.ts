import ProductEditor from './product-editor';
import ProductManager from './product-manager';
import TransformedView from './transformed-view';
import { productReducer } from './product-reducers';

export interface Product {
    productName: string;
    colour: string
    price: string;
}

export { ProductEditor, ProductManager, TransformedView, productReducer };