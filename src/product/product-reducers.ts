import { Reducer } from 'redux';
import { ProductAction, ProductActionTypes } from './product-actions';
import { Product } from './index';

export interface ProductState {
    [key: string]: Product;
}

const initialState: ProductState = {
    'Apple iPhone 6s': {
        productName: 'Apple iPhone 6s',
        colour: 'Anthracite',
        price: 'CHF 769'
    },
    'Samsung Galaxy S8': {
        productName: 'Samsung Galaxy S8',
        colour: 'Midnight Black',
        price: 'CHF 569'
    },
    'Huawei P9': {
        productName: 'Huawei P9',
        colour: 'Mystic Silver',
        price: 'CHF 272'
    }
};

export const productReducer: Reducer<ProductState, ProductAction> =
    (state: ProductState = initialState, action: ProductAction) => {
    switch (action.type) {
        case ProductActionTypes.SUBMIT_PRODUCT:
            return {
                ...state,
                [action.product.productName]: action.product
            };
        case ProductActionTypes.DELETE_PRODUCT:
            return Object
                .keys(state)
                .filter((key: string) => key !== action.product.productName)
                .reduce((revisedState: ProductState, thisKey: string) => ({
                ...revisedState,
                [thisKey]: state[thisKey]
            }), {});
        default:
            return state;
    }
};