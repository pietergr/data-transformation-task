import { connect } from 'react-redux';
import { ProductForm } from './product-form';
import { Product } from './index';
import { Dispatch } from 'redux';
import { getSubmitProductAction, ProductAction } from './product-actions';

const mapDispatchToProps = (dispatch: Dispatch<ProductAction>) => ({
    onSubmitProduct: (product: Product) => dispatch(getSubmitProductAction(product))
});

export default connect(
    null,
    mapDispatchToProps
)(ProductForm);