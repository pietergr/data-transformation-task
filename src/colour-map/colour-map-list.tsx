import React from 'react';
import { FunctionComponent } from 'react';
import { Table } from 'reactstrap';
import { ColourTransformation } from './index';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import classNames from 'classnames';

type ColourMapListProps = {
    colourTransformations: ColourTransformation[];
    onDeleteColourTransformation: (colourTransformation: ColourTransformation) => void;
    onSelectColourTransformation: (colourTransformation: ColourTransformation) => void;
    selectedColourTransformation: ColourTransformation;
}

export const ColourMapList: FunctionComponent<ColourMapListProps> = ({ colourTransformations, onDeleteColourTransformation, onSelectColourTransformation, selectedColourTransformation }) => {
    return <Table hover>
        <thead>
            <tr>
                <th>Colour <strong>from</strong></th>
                <th>Colour <strong>to</strong></th>
            </tr>
        </thead>
        <tbody>
        {
            colourTransformations.map((transformation: ColourTransformation, idx: number) => (
                <tr
                    key={ idx }
                    onClick={ () => onSelectColourTransformation(transformation) }
                    className={ classNames(selectedColourTransformation.colourFrom === transformation.colourFrom ? 'table-info' : 'table-default')}>
                    <td>{ transformation.colourFrom }</td>
                    <td>{ transformation.colourTo }</td>
                    <td className="pointer text-danger" onClick={ () => onDeleteColourTransformation(transformation) }>
                        <FontAwesomeIcon icon={ faTrash }/>
                    </td>
                </tr>
            ))
        }
        </tbody>
    </Table>
};