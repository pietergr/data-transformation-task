import { Dispatch } from 'redux';
import { ColourTransformationAction, getSubmitColourTransformationAction } from './colour-transformation-actions';
import { ColourTransformation } from './index';
import { connect } from 'react-redux';
import { ColourMapForm } from './colour-map-form';
import { AppState } from '../store';
import { selectedColourTransformation } from './colour-transformation-selectors';

const mapStateToProps = (state: AppState) => ({
    selectedColourTransformation: selectedColourTransformation(state)
});

const mapDispatchToProps = (dispatch: Dispatch<ColourTransformationAction>) => ({
    onSubmitColourTransformation: (colourTransformation: ColourTransformation) => dispatch(getSubmitColourTransformationAction(colourTransformation))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ColourMapForm);