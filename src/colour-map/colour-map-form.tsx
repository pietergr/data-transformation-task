import React, { ChangeEvent, FunctionComponent, MouseEvent, useEffect, useState } from 'react';
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
import { ColourTransformation } from './index';

type ColourMapFormProps = {
    onSubmitColourTransformation: (colourTransformation: ColourTransformation) => void;
    selectedColourTransformation: ColourTransformation;
}

const emptyColourTransformation: ColourTransformation = {
    colourFrom: '',
    colourTo: ''
};

export const ColourMapForm: FunctionComponent<ColourMapFormProps> = ({ onSubmitColourTransformation, selectedColourTransformation }) => {

    const [ { colourFrom, colourTo }, setColourTransformation ] = useState<ColourTransformation>(selectedColourTransformation || emptyColourTransformation);

    const changeColourTransformation = (colourTransformationPart: {colourFrom?: string, colourTo?: string}) => {
        setColourTransformation(Object.assign({
            colourFrom,
            colourTo
        }, colourTransformationPart));
    };

    const submit = (e: MouseEvent) => {
        e.preventDefault();
        onSubmitColourTransformation({
            colourFrom,
            colourTo
        });
        setColourTransformation(emptyColourTransformation);
    };

    useEffect(() => {
        setColourTransformation(selectedColourTransformation);
    }, [selectedColourTransformation]);

    return (<Form>
            <FormGroup>
                <Label>Colour <strong>from</strong></Label>
                <Input
                    type="text"
                    value={colourFrom}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => changeColourTransformation({
                        colourFrom: e.target.value
                })}/>
            </FormGroup>
            <FormGroup>
                <Label>Colour <strong>to</strong></Label>
                <Input
                    type="text"
                    value={colourTo}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => changeColourTransformation({
                        colourTo: e.target.value
                })}/>
            </FormGroup>
            <Button
                color="info"
                disabled={!colourFrom || !colourTo}
                onClick={(e: MouseEvent<HTMLElement>) => submit(e)}>Add</Button>
        </Form>);
};