import { Reducer } from 'redux';
import { ColourTransformationAction, ColourTransformationTypes } from './colour-transformation-actions';
import { ColourTransformation } from './index';

export interface ColourTransformationState {
    [key: string]: ColourTransformation;
    selectedColourTransformation: ColourTransformation;
}

const emptyColourTransformation: ColourTransformation = {
    colourFrom: '',
    colourTo: ''
};

const initialState: ColourTransformationState = {
    selectedColourTransformation: emptyColourTransformation
};

export const colourTransformationReducer: Reducer<ColourTransformationState, ColourTransformationAction> =
    (state: ColourTransformationState = initialState, action: ColourTransformationAction) => {
    switch (action.type) {
        case ColourTransformationTypes.SUBMIT_COLOUR_TRANSFORMATION:
            return {
                ...state,
                [action.colourTransformation.colourFrom]: action.colourTransformation,
                selectedColourTransformation: emptyColourTransformation
            };
        case ColourTransformationTypes.SELECT_COLOUR_TRANSFORMATION:
            return {
                ...state,
                selectedColourTransformation: action.colourTransformation
            };
        case ColourTransformationTypes.DELETE_COLOUR_TRANSFORMATION:
            return Object.keys(state).filter((key: string) => key !== action.colourTransformation.colourFrom)
                .reduce((revisedState: ColourTransformationState, thisKey: string) => ({
                    ...revisedState,
                    [thisKey]: state[thisKey]
                }), {
                    selectedColourTransformation: emptyColourTransformation
                });
        default:
            return state;
    }
};