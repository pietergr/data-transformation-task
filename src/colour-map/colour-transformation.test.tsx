import { colourTransformationReducer, ColourTransformationState } from './colour-transformation-reducers';
import { ColourTransformation } from './index';
import {
    getDeleteColourTransformationAction,
    getSubmitColourTransformationAction
} from './colour-transformation-actions';

describe('colour transformation state', () => {

    const emptyColourTransformation: ColourTransformation = {
        colourFrom: '',
        colourTo: ''
    };

    const initialState: ColourTransformationState = {
        selectedColourTransformation: emptyColourTransformation
    };

    const newColourTransformation: ColourTransformation = {
        colourFrom: 'a',
        colourTo: 'b'
    };

    it('adds a colour transformation', () => {
        const newState = colourTransformationReducer(initialState, getSubmitColourTransformationAction(newColourTransformation));
        expect(newState).toEqual({
            selectedColourTransformation: emptyColourTransformation,
            a: {
                colourFrom: 'a',
                colourTo: 'b'
            }
        })
    });

    it('updates an existing colour transformation', () => {
        const newState = colourTransformationReducer(initialState, getSubmitColourTransformationAction(newColourTransformation));
        const updatedColourTransformation: ColourTransformation = {
            colourFrom: 'a',
            colourTo: 'newValue'
        };
        const finalState = colourTransformationReducer(newState, getSubmitColourTransformationAction(updatedColourTransformation));
        expect(finalState).toEqual({
            selectedColourTransformation: emptyColourTransformation,
            a: {
                colourFrom: 'a',
                colourTo: 'newValue'
            }
        })
    });

    it('deletes a colour transformation', () => {
        const newState = colourTransformationReducer(initialState, getSubmitColourTransformationAction(newColourTransformation));
        const finalState = colourTransformationReducer(newState, getDeleteColourTransformationAction(newColourTransformation));
        expect(finalState).toEqual({
            selectedColourTransformation: emptyColourTransformation
        })
    });

    it('does not allow a colour transformation clone', () => {
        const newState = colourTransformationReducer(initialState, getSubmitColourTransformationAction(newColourTransformation));
        const finalState = colourTransformationReducer(newState, getSubmitColourTransformationAction(newColourTransformation));
        expect(finalState).toEqual({
            selectedColourTransformation: emptyColourTransformation,
            a: {
                colourFrom: 'a',
                colourTo: 'b'
            }
        });
    });

    it('does not allow a colour transformation fork', () => {
        const forkyColourTransformation: ColourTransformation = {
            colourFrom: 'a',
            colourTo: 'forkValue'
        };
        const newState = colourTransformationReducer(initialState, getSubmitColourTransformationAction(newColourTransformation));
        const finalState = colourTransformationReducer(newState, getSubmitColourTransformationAction(forkyColourTransformation));
        expect(finalState).toEqual({
            selectedColourTransformation: emptyColourTransformation,
            a: {
                colourFrom: 'a',
                colourTo: 'forkValue'
            }
        });
    });

});