import { AppState } from '../store';
import { colourTransformationArraySelector, selectedColourTransformation } from './colour-transformation-selectors';
import { Dispatch } from 'redux';
import {
    ColourTransformationAction,
    getDeleteColourTransformationAction,
    getSelectColourTransformationAction
} from './colour-transformation-actions';
import { connect } from 'react-redux';
import { ColourMapList } from './colour-map-list';
import { ColourTransformation } from './index';

const mapStateToProps = (state: AppState) => ({
    colourTransformations: colourTransformationArraySelector(state),
    selectedColourTransformation: selectedColourTransformation(state)
});

const mapDispatchToProps = (dispatch: Dispatch<ColourTransformationAction>) => ({
    onDeleteColourTransformation: (colourTransformation: ColourTransformation) => dispatch(getDeleteColourTransformationAction(colourTransformation)),
    onSelectColourTransformation: (colourTransformation: ColourTransformation) => dispatch(getSelectColourTransformationAction(colourTransformation)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ColourMapList);