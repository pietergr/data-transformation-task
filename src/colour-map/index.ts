import { colourTransformationReducer} from './colour-transformation-reducers';
import ColourMapManager from './colour-map-manager';
import ColourMapEditor from './colour-map-editor';

export interface ColourTransformation {
    colourFrom: string;
    colourTo: string;
}

export { ColourMapManager, ColourMapEditor, colourTransformationReducer };