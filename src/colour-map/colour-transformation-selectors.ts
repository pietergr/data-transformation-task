import { AppState } from '../store';
import { createSelector } from 'reselect';
import { ColourTransformationState } from './colour-transformation-reducers';

export const colourTransformationSelector = (state: AppState) => state.transformations;

export const colourTransformationArraySelector = createSelector(
    colourTransformationSelector,
    (colourTransformation: ColourTransformationState) => colourTransformation
        ? Object.keys(colourTransformation)
            .filter((key: string) => key !== 'selectedColourTransformation')
            .map((key: string) => colourTransformation[key])
        : []
);

export const selectedColourTransformation = createSelector(
    colourTransformationSelector,
    (colourTransformation: ColourTransformationState) => colourTransformation.selectedColourTransformation
);