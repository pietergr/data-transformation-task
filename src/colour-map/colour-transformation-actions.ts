import { Action, ActionCreator } from 'redux';
import { ColourTransformation } from './index';

export enum ColourTransformationTypes {
    SUBMIT_COLOUR_TRANSFORMATION = '[ColourTransformationActions] Submit Colour Transformation',
    SELECT_COLOUR_TRANSFORMATION = '[ColourTransformationActions] Select Colour Transformation',
    DELETE_COLOUR_TRANSFORMATION = '[ColourTransformationActions] Delete Colour Transformation'
};

export interface ColourTransformationAction extends Action {
    colourTransformation: ColourTransformation
}

export const getSubmitColourTransformationAction: ActionCreator<ColourTransformationAction> = (colourTransformation: ColourTransformation) => ({
    type: ColourTransformationTypes.SUBMIT_COLOUR_TRANSFORMATION,
    colourTransformation
});

export const getSelectColourTransformationAction: ActionCreator<ColourTransformationAction> = (colourTransformation: ColourTransformation) => ({
    type: ColourTransformationTypes.SELECT_COLOUR_TRANSFORMATION,
    colourTransformation
});

export const getDeleteColourTransformationAction: ActionCreator<ColourTransformationAction> = (colourTransformation: ColourTransformation) => ({
    type: ColourTransformationTypes.DELETE_COLOUR_TRANSFORMATION,
    colourTransformation
});