import React, { FunctionComponent } from 'react';
import { Col, Container, Row } from 'reactstrap';
import { Tabs } from './tabs';
import { TransformedView } from '../product';

export const ControlPanel: FunctionComponent<{}> = () => {
    return (
        <Container fluid>
            <Row>
                <Col md="6">
                    <h3 className="display-4">Dictionary & Source management</h3>
                    <Tabs/>
                </Col>
                <Col md="6">
                    <h3 className="display-4">Transformed result</h3>
                    <TransformedView/>
                </Col>
            </Row>
        </Container>
    );
};