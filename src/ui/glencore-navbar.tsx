import React, { FunctionComponent } from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';
import './navbar.scss';

export const GlencoreNavbar: FunctionComponent<{}> = () => {
    return (
        <Navbar color="info" className="text-white">
            <NavbarBrand><h1>Glencore Data Transformation</h1><p>Pieter Grobler</p></NavbarBrand>
        </Navbar>
    );
};