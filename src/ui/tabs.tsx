import React, { FunctionComponent, useState } from 'react';
import { Col, Jumbotron, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap';
import classNames from 'classnames';
import { ColourMapEditor, ColourMapManager } from '../colour-map';
import { ProductEditor, ProductManager } from '../product';

export const Tabs: FunctionComponent<{}> = () => {

    const [activeTab, setActiveTab] = useState<string>('1');

    return (
        <>
            <Nav tabs>
                <NavItem className="pointer">
                    <NavLink
                        className={classNames({ active: activeTab === '1' })}
                        onClick={() => {
                            setActiveTab('1');
                        }}>
                        Colour dictionaries
                    </NavLink>
                </NavItem>
                <NavItem className="pointer">
                    <NavLink
                        className={classNames({ active: activeTab === '2' })}
                        onClick={() => {
                            setActiveTab('2');
                        }}>
                        Source product data
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                    <Row>
                        <Col>
                            <Jumbotron>
                                <ColourMapEditor/>
                            </Jumbotron>
                            <ColourMapManager/>
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tabId="2">
                    <Row>
                        <Col>
                            <Jumbotron>
                                <ProductEditor/>
                            </Jumbotron>
                            <ProductManager/>
                        </Col>
                    </ Row>
                </ TabPane>
            </TabContent>
        </>
    );

};