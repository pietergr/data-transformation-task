## Hi!

Thank you for considering my application. 

You can run the project by running:

`npm install`

and then

`yarn start`

The tests can be run with

`yarn test`

# State management approach

I have selected Redux in this instance. I keep track of two normalised types of entities: products and colour transformations. 
The resulting transformed product data is derived, in close proximity to the benefits that MobX provides.

# Validation

I have not explicitly implemented validation, but instead opted for using a model that disables corruption by nature of its
structure, i.e. an object as opposed to an array. The tests cover these cases to ensure that these assumptions about avoiding
clones and forks are correct.

# Function

The colour transformation dictionaries can be CRUD operated as specified. I have additionally added creation and deletion
on the source product data.

# Unit tests

I have opted for a selection of tests that cover validation of the state. Of course there are many more to add in a 
commercial/production environment, including E2E ones and snapshots.

## Vielen Dank!
Pieter Grobler

